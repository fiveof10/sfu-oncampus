//
//  SFUHourlyForecastTableViewController.h
//  Multi Storyboard
//
//  Created by Abram Wiebe on 2015-03-08.
//  Copyright (c) 2015 Simon Fraser University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFUHourlyForecastTableCell.h"
@interface SFUHourlyForecastTableViewController : UITableViewController


@end
