//
//  SFUWebViewController.h
//  SFU OnCampus
//
//  Created by Abram Wiebe on 2015-02-21.
//  Copyright (c) 2015 Simon Fraser University. All rights reserved.
//

#import "DetailViewController.h"

@interface SFUWebViewController : DetailViewController <UIWebViewDelegate>

@property NSURL* startURL;
@property NSMutableDictionary* completionHandlerDictionary;
@property NSURLSession* defaultSession;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *forwardButton;

/**
 Navigate the web view to s specific page
 */
-(void)displayPageForURL:(NSURL*)url inApp:(BOOL)showInApp;
@end
