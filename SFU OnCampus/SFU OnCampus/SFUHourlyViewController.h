//
//  SFUHourlyViewController.h
//  OnCampus
//
//  Created by Kevin Grant on 2015-03-07.
//  Copyright (c) 2015 Simon Fraser University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFUHourlyViewController : UITableViewController

@end
